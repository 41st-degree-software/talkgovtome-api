<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private $helper;

    public function __construct()
    {
        $this->helper = App('Helper');
    }

    public function postUser(Request $request)
    {
        $this->checkRequest($request);

        $user = User::firstOrCreate(['facebook_id' => $request->get('facebook_id')]);

        $user->first_name = $request->get("first_name");
        $user->last_name = $request->get("last_name");
        $user->dob = $request->get("dob");
        $user->gender = $request->get("gender");
        $user->hometown = $request->get("hometown");
        $user->friends = $request->get("friends");

        $user->save();

        $response = json_encode(['status' => "success", "message" => ""]);

        return ($response);
    }

    public function postLocation(Request $request)
    {
        $this->checkRequest($request);

        try {
            $user = User::where('facebook_id', '=', $request->get("facebook_id"))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return $this->helper->throwError("database", "Unable to find user.");
        }

        $user->latitude = $request->get("latitude");
        $user->longitude = $request->get("longitude");

        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . $request->get("latitude") .
            "," . $request->get("longitude") . "&key=AIzaSyCM2X1QImSkIc4R2Qo2FJPrsd_EQB94WmU&result_type=postal_code|locality";

        $location = json_decode(file_get_contents($url));
        $results = $location->results;
        $components = $results[0]->address_components;

        $suburb = $components[0]->long_name;
        $postcode = $components[3]->long_name;

        $user->current_postcode = $postcode;
        $user->current_suburb = $suburb;

        $user->save();

        $response = json_encode(['status' => "success", "message" => ""]);

        return ($response);
    }

    private function checkRequest(Request $request)
    {
        // Check if the request is valid.
    }
}
