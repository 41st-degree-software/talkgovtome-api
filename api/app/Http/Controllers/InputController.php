<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class InputController extends Controller
{

    private $helper;
    private $voice;

    public function __construct()
    {
        $this->helper = App('Helper');
        $this->voice = App('Voice');
    }

    public function postInput(Request $request)
    {
        $sentence = $request->get("input");

        try {
            $user = User::where('facebook_id', '=', $request->get("facebook_id"))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return $this->helper->throwError("database", "Unable to find user.");
        }

        $topic = $this->voice->findTopic($sentence);

        switch ($topic) {
            case "abusive":
                $answer = $this->voice->abusive->getReply($user);
                $suggestion = $this->voice->suggestTopic($user);
                break;
            case "trash":
                $answer = $this->voice->trash->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "City of Hobart and Launceston Litter Bins";
                break;
            case "tv":
                $answer = $this->voice->abc->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "Television Broadcast Data Archive 1978-2011";
                break;
            case "hospitals":
                $answer = $this->voice->hospital->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "Australian Hospital Statistics 2012-13";
                break;
            case "grim":
                $answer = $this->voice->grim->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "General Record of Incidence of Mortality (GRIM)";
                break;
            case "charity":
                $answer = $this->voice->charity->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "ACNC 2013 Annual Information Statement Data";
                break;
            case "toilet":
                $answer = $this->voice->toilet->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "National Public Toilet Map";
                break;
            case "names":
                $answer = $this->voice->name->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "Baby Names in Tasmania 2010 to 2014";
                break;
            case "detention":
                $answer = $this->voice->detention->getFact($user);

                $user->last_topic = $topic;
                $user->save();

                $suggestion = $this->voice->suggestTopic($user);
                $topic = "Youth detention population in Australia 2013";
                break;
            default:
                $answer = "I'm sorry I don't understand, why don't you ask me about one of the following topics.";
                $topics = $this->voice->topics;

                $suggestion = "";

                foreach($topics as $t){
                    $suggestion .= $t . ", ";
                }

                $suggestion = rtrim($suggestion, ", ");
                $topic = "";
                break;
        }

        $return = json_encode(["answer" => $answer, "suggestion" => $suggestion, "topic" => $topic]);

        return $return;
    }
}