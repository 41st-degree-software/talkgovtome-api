<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('user', function () {
    return view('welcome');
});
Route::post('user', 'UserController@postUser');

Route::get('user/location', function () {
    return view('welcome');
});
Route::post('user/location', 'UserController@postLocation');

Route::get('input', function () {
    return view('welcome');
});
Route::post('input', 'InputController@postInput');

