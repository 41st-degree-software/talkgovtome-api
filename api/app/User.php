<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    protected $table = 'users';

    protected $fillable = [
        'facebook_id', 'first_name', 'last_name', 'dob', 'gender',
        'latitude', 'longitude', 'current_postcode', 'current_suburb', 'hometown', 'friends'];

    protected $hidden = [''];
}