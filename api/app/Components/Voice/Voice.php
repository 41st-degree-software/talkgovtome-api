<?php

namespace App\Components\Voice;

use App\Components\Voice\Topics\AbcTopic;
use App\Components\Voice\Topics\AbusiveTopic;
use App\Components\Voice\Topics\BabyNameTopic;
use App\Components\Voice\Topics\CharityTopic;
use App\Components\Voice\Topics\ToiletTopic;
use App\Components\Voice\Topics\TrashTopic;
use App\Components\Voice\Topics\HospitalTopic;
use App\Components\Voice\Topics\GrimTopic;
use App\Components\Voice\Topics\NameTopic;
use App\Components\Voice\Topics\DetentionTopic;

class Voice
{
    public $abusive;
    public $trash;
    public $abc;
    public $hospital;
    public $grim;
    public $charity;
    public $toilet;
    public $name;
    public $detention;

    public $topics = ["tv", "trash", "hospitals", "toilets", "grim", "charity", "names", "detention"];

    public function __construct()
    {
        $this->abusive = new AbusiveTopic();
        $this->trash = new TrashTopic();
        $this->abc = new AbcTopic();
        $this->hospital = New HospitalTopic();
        $this->grim = New GrimTopic();
        $this->charity = new CharityTopic();
        $this->toilet = new ToiletTopic();
        $this->name = new NameTopic();
        $this->detention = new DetentionTopic();
    }

    public function findTopic($sentence)
    {
        if ($this->checkKeywords($sentence, $this->abusive->keywords)) {
            return "abusive";
        }
        if ($this->checkKeywords($sentence, $this->trash->keywords)) {
            return "trash";
        }
        if ($this->checkKeywords($sentence, $this->abc->keywords)) {
            return "tv";
        }
        if ($this->checkKeywords($sentence, $this->hospital->keywords)) {
            return "hospitals";
        }
        if ($this->checkKeywords($sentence, $this->grim->keywords)) {
            return "grim";
        }
        if ($this->checkKeywords($sentence, $this->charity->keywords)) {
            return "charity";
        }
        if ($this->checkKeywords($sentence, $this->toilet->keywords)) {
            return "toilet";
        }
        if ($this->checkKeywords($sentence, $this->name->keywords)) {
            return "names";
        }
        if ($this->checkKeywords($sentence, $this->detention->keywords)) {
            return "detention";
        }
    }

    public function suggestTopic($user)
    {
        $random = rand(1, 3);
        $topic = $this->topics[rand(0, 1)];

        while($topic == $user->last_topic){
            $topic = $this->topics[rand(0, 1)];
        }

        switch ($random) {
            case 1:
                $suggest = "You should ask me about the " . $topic;
                break;
            case 2:
                $suggest = "I bet you might find the " . $topic . " interesting";
                break;
            case 3:
                $suggest = "You could ask me what I know about the " . $topic;
                break;
        }

        return $suggest;
    }

    private function checkKeywords($sentence, $keywords)
    {
        foreach ($keywords as $keyword) {
            if (str_contains(strtolower($sentence), strtolower($keyword))) {
                return true;
            }
        }
        return false;
    }
}