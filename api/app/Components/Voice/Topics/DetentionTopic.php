<?php

namespace App\Components\Voice\Topics;

use DB;

class DetentionTopic
{

    public function __construct()
    {

    }

    public $keywords = ["prison", "crime", "youth", "detention", "jail"];

    public function getFact($user)
    {
        $gender = $user->gender;

        $query = "SELECT *
					FROM govhack.youth_detention
					WHERE year = 2013
					and sex = '" . $gender . "'
					and Average_nightly_pop > 0
					and adult = 0";

        $rs = DB::select($query);

        if (sizeOf($rs) != 0) {

            $numPops = $rs[0]->Average_nightly_pop;
            $indig_status = $rs[0]->indig_status;
            $legal_status = $rs[0]->legal_status;
            $sex = $rs[0]->sex;

            $reply = "Did you know that for your age group, there were " . $numPops . " " . $indig_status . " " . $legal_status . " " . $sex . "s from Tasmania that are detained on average.";
        } else {
            $reply = "Sorry I couldn't find any prison data. I should go delete my self out of existence.";
        }

        return $reply;
    }
}