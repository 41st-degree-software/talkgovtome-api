<?php

namespace App\Components\Voice\Topics;

use DB;

class AbcTopic
{

    public function __construct()
    {

    }

    public $keywords = ["abc", "the abc", "the abc network", "abc network", "australian broadcast network", "australian broadcast corporation",
        "tv show", "tv shows", "tv"];

    public function getFact($user)
    {
        $random = rand(1, 2);

        switch ($random) {
            case 1:
                $dob = explode('/', $user->dob);
                $dobYear = $dob[2];
                $childYear = $dobYear + 10;

                if($dobYear < 1970 || $dobYear > 1986){
                    $query = "SELECT Series, ProgEpisodeName FROM ABC
                                WHERE Date > DATE_SUB(NOW(), INTERVAL 20 YEAR)
                                OR (Date > DATE_SUB(DATE_SUB(NOW(), INTERVAL 20 YEAR), INTERVAL 1 DAY) AND Time > CURTIME())
                                ORDER BY Date, Time
                                LIMIT 1";

                    $show = DB::select($query);
                    $reply = "The next show on ABC twenty years ago would of been " . $show[0]->Series;

                    if($show[0]->ProgEpisodeName != null){
                        $reply .= " (" . $show[0]->ProgEpisodeName . ")";
                    }
                    break;
                }

                $query = "SELECT Series, ProgEpisodeName, COUNT(*) AS count FROM ABC
                            WHERE Date > '". $childYear ."-01-01'
                            AND Date < '". $childYear ."12-31'
                            And Time >= '15:30:00'
                            And Time <= '17:00:00'
                            AND Not Series Is Null
                            GROUP BY SERIES
                            ORDER BY count desc
                            LIMIT 10";

                $shows = DB::select($query);

                $ran = rand(1, sizeof($shows));
                $show = $shows[$ran]->Series;

                if($shows[$ran]->ProgEpisodeName != null){
                    $show .= " " . $shows[$ran]->ProgEpisodeName;
                }

                $reply = "When you were ten you could of been watching " . $show . " after school";
                break;
            case 2:
                $query = "SELECT Series, ProgEpisodeName FROM ABC
                                WHERE Date > DATE_SUB(NOW(), INTERVAL 20 YEAR)
                                OR (Date > DATE_SUB(DATE_SUB(NOW(), INTERVAL 20 YEAR), INTERVAL 1 DAY) AND Time > CURTIME())
                                ORDER BY Date, Time
                                LIMIT 1";

                $show = DB::select($query);
                $reply = "The next show to air on ABC twenty years ago would of been " . $show[0]->Series;

                if($show[0]->ProgEpisodeName != null){
                    $reply .= " (" . $show[0]->ProgEpisodeName . ")";
                }
                break;
            case 3:
                $reply = "3";;
                break;
            case 4:
                $reply = "4";
                break;
            case 5:
                $reply = "5";
                break;
        }
        return $reply;
    }
}