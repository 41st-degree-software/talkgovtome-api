<?php

namespace App\Components\Voice\Topics;

use DB;

use App\Components\Helper\Helper;

class TrashTopic
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper();
    }

    public $keywords = ["garbage", "rubbish", "recycling", "bin", "trash", "ben", "bean"];

    public function getFact($user)
    {
        $lati = $user->latitude;
        $long = $user->longitude;

        if ($this->helper->calculateDistance(-42.8806, 147.3250, $lati, $long) < 10) {
            $query = "SELECT latitude, longitude, (ABS(latitude - " . $lati . ") + ABS(longitude - " . $long . "))
                        AS distance
                        FROM hobart_trash
                        ORDER BY distance
                        LIMIT 1";

            $bin = Db::select($query);

            $dist = $this->helper->calculateDistance($bin[0]->latitude, $bin[0]->longitude, $lati, $long) * 1000;
            $reply = "The nearest public bin is located only " . round($dist) . " metres from you.";

        } else if ($this->helper->calculateDistance(-41.4419, 147.1450, $lati, $long) < 10) {
            $query = "SELECT latitude, longitude, Class, (ABS(latitude - " . $lati . ") + ABS(longitude - " . $long . "))
                        AS distance
                        FROM trash
                        ORDER BY distance
                        LIMIT 1";

            $bin = Db::select($query);

            $dist = $this->helper->calculateDistance($bin[0]->latitude, $bin[0]->longitude, $lati, $long) * 1000;

            $reply = "The nearest public bin is located only " . round($dist) . " metres from you.";

        } else {
            $reply = "Please don't talk dirty to me.";
        }

        return $reply;
    }
}