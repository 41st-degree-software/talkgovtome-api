<?php

namespace App\Components\Voice\Topics;

use DB;
use App\Components\Helper\Helper;

class ToiletTopic
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper();
    }

    public $keywords = ["toilet", "dunny", "loo", "outhouse", "busting", "thunderbox", "piss", "throne", "shit", "got to go"];

    public function getFact($user)
    {
        $lati = $user->latitude;
        $long = $user->longitude;

        $query = "SELECT Latitude, Longitude, (ABS(Latitude - " . $lati . ") + ABS(Longitude - " . $long . ")) AS distance
                    FROM toilet
                    WHERE MALE = 'TRUE'
                    OR FEMALE = 'TRUE'
                    ORDER BY distance
                    LIMIT 1";

        $toilet = DB::select($query);

        $distance = $this->helper->calculateDistance($toilet[0]->Latitude, $toilet[0]->Longitude, $lati, $long) * 1000;
        $distance = round($distance);

        $random = rand(1, 4);

        switch ($random) {
            case 1:
                $reply = "Busting to go?  There is a public toilet located only " . $distance . " metres from you.";
                break;
            case 2:
                $reply = "Need to dump a load? There is a public toilet located only " . $distance . " metres from you.";
                break;
            case 3:
                $reply = "Your nearest thunderbox is only " . $distance . " metres from you.";
                break;
            case 4:
                $reply = "A toilet is only " . $distance . " metres from you, but I'll let you find the direction.";
                break;
        }

        return $reply;
    }
}