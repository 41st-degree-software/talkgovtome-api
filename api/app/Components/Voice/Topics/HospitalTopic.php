<?php

namespace App\Components\Voice\Topics;

use DB;

class HospitalTopic
{

    public function __construct()
    {

    }

    public $keywords = ["bleeding", "emergency", "hospital", "surgery", "operation", "lobotomy", "in labour", "doctor", "nurse", "hospitals"];

    public function getFact($user)
    {
        $lati = $user->latitude;
        $long = $user->longitude;

        $query = "SELECT distinct hospitals.* FROM hospitals
                    LEFT OUTER JOIN postcode ON hospitals.Postcode = postcode.postcode
                    WHERE Not latitude Is Null
                    And Not longitude Is Null
                    ORDER BY (ABS(latitude - " . $lati . ") + ABS(longitude - " . $long . "))
                    LIMIT 1";

        $hosp = DB::select($query);

        if ($user->friends < $hosp[0]->Number_of_available_beds) {
            $reply = "There are " . $hosp[0]->Number_of_available_beds . " beds at your nearest hospital, " . $hosp[0]->Hospname . ". That's more than your number of friends on Facebook.";
        } else {
            $reply = "Your nearest hospital is the " . $hosp[0]->Hospname;
        }

        return $reply;
    }
}
