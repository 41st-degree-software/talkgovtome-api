<?php

namespace App\Components\Voice\Topics;

class AbusiveTopic
{

    public function __construct()
    {

    }

    public $keywords = ["f*** you", "i hate you", "i really hate you"];

    public function getReply($user)
    {
        $random = rand(1, 3);

        switch ($random) {
            case 1:
                $reply = "Please be nice " . $user->first_name;
                break;
            case 2:
                $reply = $user->first_name . " that was rude!";
                break;
            case 3:
                $reply = "Play nice or i'll tell your parents " . $user->first_name;
                break;
        }
        return $reply;
    }
}