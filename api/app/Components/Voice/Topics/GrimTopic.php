<?php

namespace App\Components\Voice\Topics;

use DB;

class GrimTopic
{

    public function __construct()
    {

    }

    public $keywords = ["die", "death", "mortality", "sad", "horrible", "bad news", "dying", "kick the bucket"];

    public function getFact($user)
    {
        $dob = explode('/', $user->dob);

        $sex = ucfirst($user->gender) . "s";
        $age = 2015 - $dob[2];

        $query = "SELECT * FROM grim
                    WHERE Year = 2012
                    AND SEX = '" . $sex . "'
                    AND Age > " . $age . "
                    AND deaths > 0
                    ORDER BY Age
                    LIMIT 20";

        $deaths = DB::select($query);

        $random = rand(1, sizeof($deaths));

        $cause_of_death = $deaths[$random]->cause_of_death;
        $cause_of_death = substr($cause_of_death, 0, strpos($cause_of_death, "("));

        $num_deaths = $deaths[$random]->deaths;

        $reply = "Did you know that annually, " . $num_deaths . " people of your age and gender have died of " . $cause_of_death . " in Australia?";

        return $reply;
    }
}