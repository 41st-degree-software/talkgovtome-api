<?php

namespace App\Components\Voice\Topics;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

class NameTopic
{

    public function __construct()
    {

    }

    public $keywords = ["name", "baby", "popular", "unique", "baby name", "babies", "names"];

    public function getFact($user)
    {
        $name = $user->first_name;
        $query = "SELECT SUM(Number) AS NumNames
					FROM babynames
					WHERE name like '" . $name . "%'";

        try {
            $babyName = DB::select($query);
            $nameCount = $babyName[0]->NumNames;

            $reply = "You're unique! Just like the " . $nameCount . " other people called " . $name . " in Tasmania.";
        } catch (ModelNotFoundException $e) {
            $reply = "Wow! You're the only " . $name . ". What a weird name!";
        }

        return $reply;
    }
}