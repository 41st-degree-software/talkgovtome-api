<?php

namespace App\Components\Voice\Topics;

use DB;

class CharityTopic
{

    public function __construct()
    {

    }

    public $keywords = ["donate", "support", "charity", "give my time", "can I help", "what can I do"];

    public function getFact($user)
    {
        $lati = $user->latitude;
        $long = $user->longitude;

        $query = "SELECT distinct charities.* FROM charities
                    LEFT OUTER JOIN postcode ON charities.Postcode = postcode.postcode
                    WHERE Not latitude Is Null
                    And Not longitude Is Null
                    AND charities.State = 'TAS'
                    ORDER BY (ABS(latitude - " . $lati . ") + ABS(longitude - " . $long . "))
                    LIMIT 20";

        $charity = DB::select($query);

        $randomSwitch = rand(1, 4);
        $random = rand(1, 20);

        //TODO: remove random shit

        switch ($randomSwitch) {
            case 1:
                $reply = "Are you feeling charitable? The good people at " . $charity[$random]->Charity_Legal_Name . " could do with more people like you.";
                break;
            case 2:
                $reply = "Would you consider donating time or money to " . $charity[$random]->Charity_Legal_Name . "? Your contribution could make a difference.";
                break;
            case 3:
                $reply = "Have you thought about " . $charity[$random]->Charity_Legal_Name . "? Your donations could make a difference.";
                break;
            case 4:
                $reply = "Why not see the good work that " . $charity[$random]->Charity_Legal_Name . " is doing?";
                break;
        }

        return $reply;
    }
}