<?php

namespace App\Components\Helper;

class Helper
{

    public function __construct()
    {
    }

    public function throwError($with, $why)
    {
        $error = ['error' => $with, 'message' => $why];
        return json_encode($error);
    }

    public function calculateDistance($lat, $lon, $myLat, $myLon)
    {
        $R = 6371;
        $dLat = deg2rad($myLat - $lat);
        $dLon = deg2rad($myLon - $lon);
        $a = sin($dLat / 2) * sin($dLat / 2) +
            cos(deg2rad($lat)) * cos(deg2rad($myLat)) *
            sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $d = $R * $c;

        return $d;
    }
}