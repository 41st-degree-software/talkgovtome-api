<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Components\Voice\Voice;

class VoiceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make("Voice");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Voice', function () {
            return new Voice();
        });
    }
}